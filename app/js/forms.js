$(document).ready(function() {

    // Jquery Validate
    $('form').each(function() {
        $(this).validate({
            ignore: [],
            errorClass: "error",
            validClass: "success",
            errorElement: "div",
            wrapper: "span",
            rules: {
                name: {
                    required: true,
                    normalizer: function(value) {
                        return $.trim(value);
                    }
                },
                surname: {
                    required: true,
                    normalizer: function(value) {
                        return $.trim(value);
                    }
                },
                secondname: {
                    required: true,
                    normalizer: function(value) {
                        return $.trim(value);
                    }
                },
                city: {
                    required: true,
                    normalizer: function(value) {
                        return $.trim(value);
                    }
                },
                phone: {
                    required: true,
                    phone: true
                },
                email: {
                    required: true,
                    email: true,
                    normalizer: function(value) {
                        return $.trim(value);
                    }
                },
                password: {
                    required: true
                },
                personalAgreement: {
                    required: true
                },
                rules: {
                    required: true
                },
                captcha: {
                    required: true
                }
            },
            messages: {
                phone: {
                    phone: "Укажите номер телефона"
                },
                email: {
                    email: "Введите правильный email"
                }
            }
        });

        jQuery.validator.addMethod("phone", function(value, element) {
            return this.optional(element) || /\+7\(\d+\)\d{3}-\d{2}-\d{2}/.test(value);
        });
    });

    // Btn-Disable
    function btnDis() {
        var popup = $('.popup');

        popup.each(function() {
            var validate = $(this).find('input', 'textarea');
            var validateForm = $(this).find('form');
            var btn = $(this).find('.btn');

            validate.on('blur keyup click', function() {
                var checked = $('input:checked').length;
                if (validateForm.valid() && checked) {
                    btn.prop('disabled', false).removeClass('btn_disable');
                }
                if (validateForm.valid()) {
                    btn.prop('disabled', false).removeClass('btn_disable');
                } else {
                    btn.prop('disabled', 'disabled').addClass('btn_disable');
                }
            });
        });
    };
	btnDis();

    // Masked Phone
	$("input[type='tel']").mask("+7(999)999-99-99");
	
});