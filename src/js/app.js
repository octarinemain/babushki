$(document).ready(function() {

    // Home Slider
    var homeSwipper = new Swiper('.conditions__content', {
        slidesPerView: 3,
        allowTouchMove: false,
        navigation: {
            nextEl: '.swiper-button-next',
            prevEl: '.swiper-button-prev'
        },
        breakpoints: {
            992: {
                slidesPerView: 1,
                allowTouchMove: true,
                spaceBetween: 60
            },
            480: {
                slidesPerView: 1,
                allowTouchMove: true,
                spaceBetween: 80
            }
        }
    });

    // Rules Swiper
    var productSwiper = new Swiper('.product-slider', {
        slidesPerView: 3,
        spaceBetween: 25,
        navigation: {
            nextEl: '.swiper-button-next',
            prevEl: '.swiper-button-prev'
        },
        breakpoints: {
            768: {
                slidesPerView: 2,
                spaceBetween: 40,
            },
            480: {
                slidesPerView: 1,
                spaceBetween: 40,
            }
        }
	});

	// Slider Grandmothers && auto-height
	$(function() {
		var grandSwiper = new Swiper('#grandmothers-slider', {
			slidesPerView: 3,
			navigation: {
				nextEl: '.swiper-button-next',
				prevEl: '.swiper-button-prev'
			},
			breakpoints: {
				1200: {
					slidesPerView: 2
				},
				768: {
					slidesPerView: 1
				}
			}
		});

		var boxEl = $('#grandmothers-slider').find('.grandmothers-slider__item');
		var minHeight = 430;

		boxEl.each(function() {
			if ($(this).outerHeight() > minHeight) {
				minHeight = $(this).outerHeight();
				boxEl.css('min-height', minHeight);
			}
		});
	});

	// Slider-card
	$(function() {
		var galleryPop = $('#gallery-card-slider');
		var whom = galleryPop.find('.title span');
		var from = galleryPop.find('.title p');
		var textCongratulate = galleryPop.find('.form__text');
		var congratulate = $('#congratulate-wrap');
		var btn = congratulate.find('.congratulate-box__all');
		var cardSlider = new Swiper('.slide-card', {
			slidesPerView: 1,
			navigation: {
				nextEl: '.swiper-button-next',
				prevEl: '.swiper-button-prev'
			},
			breakpoints: {
				768: {
					spaceBetween: 40,
				}
			}
		});

		btn.on('click', function() {
			var textWhom = $(this).closest('.congratulate-box').find('.congratulate-box__heading_for').text();
			var textFrom = $(this).closest('.congratulate-box').find('.congratulate-box__heading_from').text();
			var textCon = $(this).closest('.congratulate-box').find('.congratulate-box__text').text();

			whom.text(textWhom);
			from.text(textFrom);
			textCongratulate.text(textCon);
		});
	});
	
	// Choose slider
	var chooseSlider = new Swiper('.slide-choose', {
		slidesPerView: 1,
		navigation: {
            nextEl: '.swiper-button-next',
            prevEl: '.swiper-button-prev'
		},
		pagination: {
			el: '.swiper-pagination',
			type: 'bullets',
			clickable: true
		},
	});

    // Cookies
    function cookies() {
        let cookies = $('.cookies');
        let data = sessionStorage.getItem('cookie');
        if (data === null) {
            cookies.addClass('js-show');
        }

        $('.cookies .btn').on('click', function(e) {
            e.preventDefault();
            cookies.removeClass('js-show');
            sessionStorage.setItem('cookie', 'ok');
        });
    }
    cookies();

    // PopUp
    function popUp() {
        $('.js-popup-button').on('click', function() {
            $('.popup').removeClass('js-popup-show');
            var popupClass = '.' + $(this).attr('data-popupShow');
            $(popupClass).addClass('js-popup-show');
            $('body').addClass('no-scroll');
        });
        closePopup();
    }

    // Close PopUp
    function closePopup() {
        $('.js-close-popup').on('click', function() {
            $('.popup').removeClass('js-popup-show');
            $('body').removeClass('no-scroll');
        });

        $('.popup').on('click', function(e) {
            var div = $(".popup__wrap");
            if (!div.is(e.target) && div.has(e.target).length === 0) {
                $('.popup').removeClass('js-popup-show');
                $('body').removeClass('no-scroll');
            }
        });
    }
    popUp();

    // Select
    $('select').select2();

    // Scroll
    $('.winners__table').scrollbar();
    $('.personal-table').scrollbar();
    $('.text-download-wrap').scrollbar();
	$('.photo-download-wrap').scrollbar();
	
	// Tabs
    function tabs() {
        var tabsContent = $('.content-tabs-wrap .content-tabs');

        // Reset
        $('.nav-tabs li').eq(0).addClass('active');
        tabsContent.eq(0).addClass('active');

        // Tabs Main Action
        $('.nav-tabs').on('click', 'li', function() {
            var i = $(this).index();
            $('.nav-tabs li').removeClass('active');
            $(this).addClass('active');
            tabsContent.removeClass('active');
            tabsContent.eq(i).addClass('active');
        });
    }
    tabs();

    // Burger Menu
    function toggleMobMenu() {
        $('.header__burger').on('click', function() {
            $(this).toggleClass('active');
            $('.nav').toggleClass('active');

            $(document).on('click', function(e) {
                var div = $(".nav, .header__burger");
                if (!div.is(e.target) && div.has(e.target).length === 0) {
                    $('.header__burger').removeClass('active');
                    $('.nav').removeClass('active');
                }
            });
            return false;
        });
    }
    toggleMobMenu();

    // Download-check
    $(function() {
        var wrapDownload = $('#download-wrap');
        var btnDownload = $('#photo-download');
        function readURL(input) {
            if (input.files && input.files[0]) {
                var reader = new FileReader();
                reader.onload = function (e) {
                    var newBlock = $('<div>').attr('class', 'photo-download-wrap__check');
                    var delImg = $('<div>').attr('class', 'photo-del').appendTo(newBlock);
                    var img = $('<img>').attr({
                        'src': e.target.result,
                        'alt': 'photo-check'
                    }).appendTo(newBlock);

					wrapDownload.append(newBlock);
					
					var cloneInput = $('#file-photo-check')
					.clone()
					.attr({
						'id':'',
						'class':'file-photo-check input-for-img'
					})
					.appendTo(newBlock);
					
                    var maxNewBlock = $('#download-wrap').find('.photo-download-wrap__check').length;
                    if (maxNewBlock > 4) {
                        btnDownload.addClass('file-photo_none');
                    }

                    var deletePhoto = wrapDownload.find('.photo-del');
                    deletePhoto.each(function () {
                        $(this).on('click', function (event) {
                            event.stopPropagation();
                            $(this).parent().remove();

                            var maxNewBlock = $('#download-wrap').find('.photo-download-wrap__check').length;
                            if (maxNewBlock <= 4) {
                                btnDownload.removeClass('file-photo_none');
                            }
                        });
                    });
                };
                reader.readAsDataURL(input.files[0]);
            };
        }

        $('#download-photo').on('change', '.file-photo-check', function () {
            readURL(this);
        });
	});

	 // Btn-Disable
	 function btnDis() {
        var popup = $('.popup');

        popup.each(function() {
			var validateI = $(this).find('input');
			var validateT = $(this).find('textarea');
            var validateForm = $(this).find('form');
            var btn = $(this).find('.btn');

            validateI.on('blur keyup click', function() {
                var checked = $('input:checked').length;
                if (validateForm.valid() && checked) {
                    btn.prop('disabled', false).removeClass('btn_disable');
                }
                if (validateForm.valid()) {
                    btn.prop('disabled', false).removeClass('btn_disable');
                } else {
                    btn.prop('disabled', 'disabled').addClass('btn_disable');
                }
			});
			
			validateT.on('blur keyup click', function() {
                if (validateForm.valid()) {
                    btn.prop('disabled', false).removeClass('btn_disable');
                } else {
                    btn.prop('disabled', 'disabled').addClass('btn_disable');
                }
			});
        });
    };
	btnDis();

	// Show more
    $(function() {
		var wrapBoxGallery = $('#congratulate-wrap');
        var boxGallery = wrapBoxGallery.find('.congratulate-box-wrap');
		var btn = $('#show-more');
		var resizeI;

		// resize function
        function onResize() {
			$('body, html').css('overflow', 'hidden');
			var width = $(window).outerWidth();
			$('body, html').css('overflow', 'visible');

			if (width >= 1200) {
				resizeI = 2;
			} else {
				resizeI = 1;
			}

			boxGallery.each(function(i) {
				if (i <= resizeI) {
					$(this).addClass('congratulate-box-wrap_active');
				} else {
					$(this).removeClass('congratulate-box-wrap_active');
				}
			});
		};
		
		// review show more
		btn.on('click', function(e) {
			e.preventDefault();
			var activeBox = wrapBoxGallery.find('.congratulate-box-wrap.congratulate-box-wrap_active');
			var lenght = boxGallery.length;
			var lenghtActive = activeBox.length;

			if (lenght > lenghtActive) {
				boxGallery.each(function(i) {
					if (i <= (lenghtActive + resizeI) && !$(this).hasClass('congratulate-box-wrap_active')) {
						$(this).addClass('congratulate-box-wrap_active');
					}
				});
			}

			lenghtActive = wrapBoxGallery.find('.congratulate-box-wrap.congratulate-box-wrap_active').length;

			if (lenght === lenghtActive) {
				btn.hide();
			}
		});
		
        var doit;
        doit = setTimeout(onResize, 400);
        window.onresize = function() {
            clearTimeout(doit);
            doit = setTimeout(onResize, 400);
		};
    });

});